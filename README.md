# Reign Design Test #


## Prerequisites ##
1. node package manager installed 
2. mongo database running in localhost in the default mongo port.

## Running the app ##
1. clone the repo and cd into it
2. cd to Server and type the command npm install and hit enter to install the packages
3. type `npm start` to run the app
4. cd to Client and type the command npm install and hit enter to run the app
5. type `npm start` to run the app and open in browser.

## Description ##
This is an app shows recently posted articles about Node.js on Hacker News:
`http://hn.algolia.com/api/v1/search_by_date?query=nodejs`
The server app should insert the data from the API into a MongoDB database and also define an REST API which the client will use to retrieve the data. 
