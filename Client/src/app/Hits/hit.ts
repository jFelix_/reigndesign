export interface Hit {
    story_id: String;
    title: String;
    author: String;
    story_url: String;
    created_at: Date;
    isDeleted: Boolean;
}