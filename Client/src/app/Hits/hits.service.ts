import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Hit } from './hit';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
};

@Injectable()
export class HitService {
    hitUrl = 'http://localhost:3001/';  // URL to web api
    private handleError: HandleError;

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler) {
        this.handleError = httpErrorHandler.createHandleError('HitService');
    }

    /** GET hit from the server */
    getHits(): Observable<Hit[]> {
        return this.http.get<Hit[]>(this.hitUrl)
            .pipe(
                catchError(this.handleError('getHits', []))
            );
    }

    deleteHit(id: String): Observable<{}> {
        const url = `${this.hitUrl}delete/${id}`;
        return this.http.delete(url, httpOptions)
            .pipe(
                catchError(this.handleError('deleteHit'))
            );
    }


}