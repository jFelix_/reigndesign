import { Component, OnInit } from '@angular/core';

import { Hit } from './hit';
import { HitService } from './hits.service';

@Component({
    selector: 'app-hits',
    templateUrl: './hits.component.html',
    providers: [HitService],
    styleUrls: ['./hits.component.css']
})
export class HitsComponent implements OnInit {
    hits: Hit[];

    constructor(private hitsService: HitService) { }

    ngOnInit() {
        this.getHits();
    }

    getHits(): void {
        this.hitsService.getHits()
            .subscribe(hits => this.hits = hits);
    }
    delete(hit: Hit): void {
        this.hits = this.hits.filter(h => h !== hit);
        this.hitsService.deleteHit(hit.story_id).subscribe();
    }
    openItem(url): void{
        window.open(url);
    }
}