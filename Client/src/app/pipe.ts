import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'niceDateFormatPipe',
})
export class niceDateFormatPipe implements PipeTransform {
    transform(value: string) {

        var _value = new Date(value);
        console.log(_value);
        var dif = Math.floor(((Date.now() - _value.valueOf()) / 1000) / 86400);
        console.log(Date.now());
        console.log(dif);
        if (dif < 30) {
            return convertToNiceDate(value);
        } else {
            var datePipe = new DatePipe("en-US");
            value = datePipe.transform(value, 'MMM dd');
            return value;
        }
    }
}

function convertToNiceDate(time: string) {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    var date = new Date(time),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        daydiff = Math.floor(diff / 86400);

    if (isNaN(daydiff) || daydiff < 0 || daydiff >= 31)
        return '';
    var extra = date.getHours() > 12 ? " pm" : " am";
    return daydiff == 0 && (
        diff < 86400 && date.getHours() + ":" + date.getMinutes() + extra) ||
        daydiff == 1 && "Yesterday" ||
        daydiff < 31 && monthNames[date.getMonth()] + " " + date.getDay();
}