import fetch from 'node-fetch';
import { Hits } from './models';
import mongoose from 'mongoose'
mongoose.Promise = Promise;
export default class dataFetchHits {
    constructor(args) {
        this.fetchData();
    }
    fetchData() {
        fetch('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            .then(res => res.json())
            .then(json => this.updateDateBase(json));
    }
    updateDateBase(body) {
        if (body != null) {
            mongoose
            .connect("mongodb://localhost:27017/ReignDesign", { useNewUrlParser: true })
            .then(() => {
                body.hits.map(hit => {
                    if ((hit.title || hit.story_title) === null) {
                        return;
                    }
                    Hits.update(
                        { story_id: hit.story_id, isDeleted: false },
                        {
                            $set: {
                                story_id: hit.story_id,
                                title: hit.title || hit.story_title,
                                created_at: hit.created_at,
                                author: hit.author,
                                story_url: hit.story_url || hit.url,
                                isDeleted: false
                            }
                        },
                        { upsert: true },
                        function (err, doc) {
                            if (err) throw err;
                            console.log(doc);
                        }
                    );
                });
            });
        }
    }
}