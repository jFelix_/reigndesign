'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _models = require('../models');

var storyController = {};
storyController.getAll = function (req, res, next) {
    _models.Story.find({ isDeleted: false }).sort({ created_at: 'desc' }).then(function (stories) {
        // console.log(stories)
        res.render('index', { title: 'HN feed', stories: stories });
        //CHANGE
    });
};
storyController.delete = function (req, res, next) {
    _models.Story.findOneAndUpdate({ story_id: req.params.id }, { isDeleted: true }).then(function () {
        res.redirect('/');
    });
};
exports.default = storyController;