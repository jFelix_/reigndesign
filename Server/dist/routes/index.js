'use strict';

var _storyController = require('../controllers/storyController');

var _storyController2 = _interopRequireDefault(_storyController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/', _storyController2.default.getAll);
router.get('/delete/:id', _storyController2.default.delete);
module.exports = router;