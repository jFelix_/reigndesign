'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;


_mongoose2.default.Promise = global.Promise;

var storySchema = new Schema({
  story_id: String,
  title: String,
  author: String,
  story_url: String,
  created_at: Date,
  isDeleted: {
    type: Boolean,
    required: true,
    default: false
  }
});

var Story = _mongoose2.default.model('Story', storySchema);
exports.default = Story;