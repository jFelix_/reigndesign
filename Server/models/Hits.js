import mongoose from 'mongoose';

const { Schema } = mongoose;

mongoose.Promise = global.Promise;

const hitSchema = new Schema({
  story_id: String,
  title:  String,
  author:  String,
  story_url:  String,
  created_at: Date,
  isDeleted: {
    type: Boolean,
    required: true,
    default: false
  }
});
const Hits = mongoose.model('hits', hitSchema);
export default Hits;
