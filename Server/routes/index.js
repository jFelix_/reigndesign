import express from 'express';
import hitsController from '../controllers/hitsController';
let router = express.Router();

/* GET users listing. */
router.get('/', hitsController.getAll);
router.delete('/delete/:id', hitsController.delete);

export default router;
